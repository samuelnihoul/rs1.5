//outer query shortcut
import qu from './query'
const q = async query => {
    try {
        const results =
            await adminDB.query(qu.Login(process.env.user, process.env.pw))
        await adminDB.query(query)
        await adminDB.query(qu.Logout(process.env.user))
        return results
    } catch (error) {
        return { error }
    }
}
export default q