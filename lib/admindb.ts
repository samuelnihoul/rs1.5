//outer query shortcut
import fauna from 'faunadb'
import qu from './query'
const q = async query => {
    const adminDB = new fauna.Client({
        timeout: 5000,
        secret: 'fnAD09ykamACBzrn1Sp0cMMAHOSeaQxwdgOEMGij',
    })
    try {
        const results =
            await adminDB.query(query)
        await adminDB.query(qu.Logout(true))
        return results
    } catch (error) {
        return { error }
    }
}
export default q