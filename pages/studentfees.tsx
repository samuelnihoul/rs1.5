import React from 'react';

const { useReducer } = require("react");
function reducer(state, action) {
    switch (action.type) {
        case 'add': return [...state, action.item]
        case 'rm': return [...state.slice(0, action.index), ...state.slice(action.index + 1, state.length)]
        default: throw new Error();
    }
}
function Fees() {
    const [state, dispatch] = useReducer(reducer, [])
    return (
        <body>
            <h1>Fees {state}</h1>
            <p>
                <button onClick={() => dispatch({ type: 'add', item: [prompt('Expense?'), prompt('Amount?')] })}>+</button>
                <button onClick={() => dispatch({ type: 'rm', index: prompt<number>('line number?') - 1 })}>-</button>

                <table>
                    <tr>
                        <th>
                            Expense
                            </th>
                        <th>
                            Amount
                            </th>

                        {state.map((item) => {
                            <td>{item[0]}</td>
                        })}
                        {state.map((item) => {
                            <td>{item[1]}</td>
                        })}



                    </tr>
                    <tr><td>Total</td></tr>

                </table>


            </p>

        </body>
    )
}
export default Fees
