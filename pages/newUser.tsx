import { useState } from 'react'
import createUser from '../lib/api/newUser'
import { redirect } from 'next/dist/next-server/server/api-utils'
const newUser = () => {
    const [pw, setpw] = useState('')
    const [nick, setnick] = useState('')
    return (
        <div>
            <select />
            <input type='name' onChange={(event) => { setnick(event.target.value) }} />
            <input type='password' onChange={(event) => { setpw(event.target.value) }} />
            <button onClick={() => { createUser(nick, pw); redirect(null, '/') }}>Submit</button>
        </div>
    )
}
export default newUser