import '../styles/globals.css'
import Link from 'next/link'

function MyApp({ Component, pageProps }) {
  return (<><Component {...pageProps} /><button><Link href="/">Logout/Home</Link></button></>)
}

export default MyApp
